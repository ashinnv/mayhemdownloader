#!/usr/bin/bash

cd dl_space

#Most common file type I run into
for i in *.opus
do
	tmpName=`trimmer_rs "$i"`
	ffmpeg -i "$i" -y -map 0:a:0 -b:a 50k "../fin/$tmpName.ultra_low_bitrate.mp3"
	rm "$i"
done

#Not sure what file type this is
for i in "dl_space/*.webm"
do
	tmpName=`trimmer_rs "$i"`
	#tmpName=`./rmft "$i"`
	ffmpeg -i "$i" -map 0:a:0 -b:a 50k "../fin/$tmpName.ultra_low_bitrate.mp3" 
	rm "$i"
done


#MPEG
for i in "dl_space/*.mpeg"
do
	tmpName=`trimmer_rs "$i"`
	#tmpName=`./rmft "$i"`
	ffmpeg -y -i "$i" -map 0:a:0 -b:a 50k "../fin/$tmpName.ultra_low_bitrate.mp3"
	rm "$i"
done

#MP3 doesn't get away
for i in *.mp3
do
	tmpName=`trimmer_rs "$i"`
	ffmpeg -i "$i" -y -map 0:a:0 -b:a 50k "../fin/$tmpName.ultra_low_bitrate.mp3"
	rm "$i"
done

#M4a
for i in "dl_space/*.m4a"
do
	tmpName=`trimmer_rs "$i"`
	#tmpName=`./rmft "$i"`
	ffmpeg -y -i pn "$i" -map 0:a:0 -b:a 50k "../fin/$tmpName.ultra_low_bitrate.mp3"
	rm "$i"
done
