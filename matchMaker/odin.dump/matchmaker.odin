package main

import "core:os"
import "core:fmt"
import "core:strings"

main::proc(){

  a,b := pull_files()

  ndx_list := search_stack(a,b)

  fmt.println(ndx_list)

}

pull_files::proc() -> (string, string){
  
  dat1, ok1 := os.read_entire_file("targs.txt", context.allocator)
  if !ok1 {
    os.exit(1)
  }
  defer delete(dat1, context.allocator)
  
  re1 := string(dat1)

  dat2, ok2 := os.read_entire_file("haystack.txt", context.allocator)
  if !ok2 {
    os.exit(1)
  }
  defer delete(dat2, context.allocator)
  
  re2 := string(dat2)

  ret1, _ := strings.split(re1, "\n")
  ret2, _ := strings.split(re2, "\n")
  return ret1, ret2 
}

search_stack::proc(needle: [dynamic]string, haystack: [dynamic]string) -> [dynamic]int {

  // List of locations in the haystack that the first letter of needle are at
  start_points: [dynamic]int

  for char,ndx in needle{
    if char == needle[0] {
      start_points.append(ndx)
    }
  }

  return start_points
}