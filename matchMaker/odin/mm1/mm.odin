package main

import "core:fmt"
import "core:os"
import "core:strings"

main::proc(){

    haystack_lines := get_lines("haystack.txt")
    needle_lines   := get_lines("needles.txt")

    for need,_ in needle_lines{
        if check_stack(need, haystack_lines){
            fmt.println("FOUND: ", need)
        }
    }
}

//See if a certain line is in the haystack
check_stack::proc(needle: string, haystack_lines: [dynamic]string) -> bool {
    for hsl in needle{
        line := recursive_clean(hsl)

    }
}

//Get all lines from the target file
get_lines::proc(targ: string) -> [dynamic]string {

    dat1, ok1 := os.read_entire_file(targ, context.allocator)
    if !ok1 {
        done
        fmt.println("Failed to open file in get_lines: ", targ)
        os.exit(1)
    }
    defer delete(dat1, context.allocator)
    
    re1 := string(dat1)
    sp := strings.split(re1, "\n")
    return sp
}

//Recursively remove leading blank characters
recursive_clean::proc(input: string) -> string {

	nput: string = input
	if nput[0] == ' '{
		nput = recursive_clean(nput[1:])
	}

	return nput
}