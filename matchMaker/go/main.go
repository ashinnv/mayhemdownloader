package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {

	splitByte := byte('\n')
	targets := getFileDat(os.Args[1])
	haystack := getFileDat(os.Args[2])

	tarsplit := strings.Split(string(targets), string(splitByte))

	for _, targ := range tarsplit {
		searchHaystack(targ, haystack)

	}

}

func searchHaystack(target, haystack string) bool {

}

func getFileDat(targ string) []byte {
	if fDat, fileError := os.ReadFile(targ); fileError != nil {
		fmt.Println(fmt.Errorf("error with getFileDat: %s", fileError.Error()))
		os.Exit(103827)
	} else {
		return fDat
	}
}
