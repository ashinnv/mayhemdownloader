#!/usr/bin/bash

types=".mp4 mp3 .m4a .webm .ogg .opus .mpeg"
for t in $types
do
    ls *.$t | parallel -j 2 './cvtr_single.sh {}'
done

