#!/usr/bin/bash

# Use eyeD3 to rename files
for file in *.mp3; do
    # Extract the artist and title from the file's metadata
    artist=$(eyeD3 "$file" | grep -oP '(?<=artist: ).*')
    title=$(eyeD3 "$file" | grep -oP '(?<=title: ).*')

    # Check if the metadata is missing
    if [[ -z "$artist" || -z "$title" ]]; then
        echo "Metadata for $file is missing. Keeping the old filename."
    else
        # Create the new filename
        new_filename="${artist}-${title}.mp3"

        # Truncate the new filename if it's longer than 50 characters
        new_filename=${new_filename:0:254}

        # Rename the file
        mv "$file" "$new_filename"
    fi
done